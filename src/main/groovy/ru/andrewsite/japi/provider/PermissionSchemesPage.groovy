package ru.andrewsite.japi.provider

import geb.Page

class PermissionSchemesPage extends Page{
    static url = "/secure/admin/ViewPermissionSchemes.jspa"

    static content = {
        table { $('tbody') }
    }

    static at = { $('header h2').text() == 'Permission schemes' }

}
