package ru.andrewsite.japi.provider

import geb.Page

class RolesPage extends Page {
    static url = "/secure/project/ViewProjectRoles.jspa"

    static content = {
        rows { $( id:'project_roles').$('tbody tr') }
    }

    static at = { $( id:'project_roles').displayed }
}
