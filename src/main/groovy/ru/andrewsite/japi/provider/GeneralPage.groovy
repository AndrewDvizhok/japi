package ru.andrewsite.japi.provider

import geb.Page

class GeneralPage extends Page{
    static url = "/secure/admin/ViewApplicationProperties.jspa"

    static content = {
        optionsTable {
            $(id:'options_table')
        }
    }

    static at = { $('div > h2').text() == 'Settings' }
}
