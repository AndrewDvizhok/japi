package ru.andrewsite.japi.provider

import geb.Browser

class SsoLogin {
    static boolean doSsoLogin(Browser browser, username, password, ssoidl, ssoidp){
        if (ssoidl == null || ssoidp == null){
            return true
        }else {
            browser.waitFor(30) { browser.$("form").displayed }
            if (browser.$(ssoidl).displayed) {
                //login-form-authenticatePassword
                browser.$("form").with {
                    browser.$(ssoidl).value(username)
                    browser.$(ssoidp).value(password)
                    $("button").click()
                }
                return true
            } else {
                return false
            }
        }
    }
}
