package ru.andrewsite.japi.provider

import geb.Page

class SystemPage extends Page{
    static url = "/secure/admin/ViewSystemInfo.jspa"

    static content = {
        systemInfoTable {
            $(id:'system_info_table')
        }
    }

    static at = { $('#server-info h3').text() == 'Server Info' }

}
