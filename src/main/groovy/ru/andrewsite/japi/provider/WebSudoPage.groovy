package ru.andrewsite.japi.provider

import geb.Page

class WebSudoPage extends Page {

    static content = {
        loginButton() { $('input',id:'login-form-submit') }
        password { $('input', id:'login-form-authenticatePassword') }
    }

    static at = { $('header > h1').text() == 'Administrator Access' && $('input',id:'login-form-submit').displayed }
}
