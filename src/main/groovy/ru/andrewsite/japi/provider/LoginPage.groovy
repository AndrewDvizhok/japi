package ru.andrewsite.japi.provider

import geb.Page

class LoginPage extends Page {
    static url = "/login.jsp"

    static content = {
        form { $(id: 'login-form') }
        loginButton() { $("input", id: "login-form-submit") }
        username { $("input", name: "os_username") }
        password { $("input", name: "os_password") }
    }

    static at = { $( id:'login-form').attr('method') == 'post' }
}
