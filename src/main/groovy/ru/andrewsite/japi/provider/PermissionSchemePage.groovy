package ru.andrewsite.japi.provider

import geb.Page

class PermissionSchemePage extends Page {
    static url = '/secure/admin/EditPermissions!default.jspa?schemeId='

    static content = {
        tables { $('tbody') }
        permissions(wait: true) { $('.title') }
        edits { $('.operation-triggers > button') }
    }

    static at = { title.startsWith("Edit Permissions") }
}
