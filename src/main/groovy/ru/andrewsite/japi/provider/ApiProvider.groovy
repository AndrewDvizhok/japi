package ru.andrewsite.japi.provider

import geb.Browser
import groovy.json.JsonSlurper
import ru.andrewsite.japi.jira.JiraGrant
import ru.andrewsite.japi.jira.JiraPermission
import ru.andrewsite.japi.jira.JiraPermissionScheme
import ru.andrewsite.japi.jira.JiraProject
import ru.andrewsite.japi.jira.JiraServer
import org.openqa.selenium.firefox.FirefoxDriver

import java.util.logging.Logger

class ApiProvider {

    private JiraServer jiraServer

    ApiProvider(JiraServer jiraServer){
        this.jiraServer = jiraServer
    }

    boolean addPermission(schemeIds, grantType, grantValue, permTypeConds, grantTypeCond, grantValueCond=null){
        Browser b = getBrowser()
        if (getSudo(b)) {
            schemeIds.each{ schemeId->
                def ps = jiraServer.permissionSchemes?.find{ it.id == schemeId }
                if (ps == null){
                    ps = new JiraPermissionScheme(schemeId)
                }
                b.go PermissionSchemePage.url+schemeId
                doWebSudo(b)

                //todo get all permissions
                ps.permissions = fillPermissionScheme(b)
                //add current permission scheme to jira server
                jiraServer.permissionSchemes.add(ps)

                if (permTypeConds?.find{ it == "all"} ){
                    permTypeConds = JiraPermissionScheme.permissionTypes
                }

                permTypeConds.each{ perm->

                    if (!hasPermission(schemeId, perm, grantType, grantValue)){

                        b.waitFor(10){b.$('tbody tr .title')}
                        def row = b.$('tbody tr')?.find{ it.$(class:'title').text() == perm }

                        if(grantTypeCond == "any"){
                            addGrant(b, row, grantType, grantValue)
                        }else{
                            //println "check perm: "+schemeId+", perm: "+perm+", gType: "+grantTypeCond+", gVal: "+ grantValueCond
                            if (hasPermission(schemeId, perm, grantTypeCond, grantValueCond)){
                                println "perm exist!"
                                addGrant(b, row, grantType, grantValue, grantTypeCond, grantValueCond)
                            }

                        }
                    }
                }

            }
            b.quit()
            return true
        }
        return false
    }

    boolean addGrant(Browser browser, row, grantType, grantValue=null, grantTypeCond=null, grantValueCond=null){
        def edit = browser.$(row).$('button')?.find{ it.text() == "Edit" }

        if (browser.$(class: 'aui-blanket').displayed) {
            //todo click on empty
            browser.driver.executeScript('$(".aui-blanket").hide();')
        }
        browser.waitFor(30) { browser.$(edit).displayed }
        edit.click()
        //click expand list
        browser.waitFor(10) { browser.$(id:'security-type-list-more-opts-btn') }
        browser.$(id:'security-type-list-more-opts-btn').click()

        def gt = browser.$('.radio label')?.find{ it.text() == grantType}
        def gtl = browser.$('.radio')?.find{ if (it.$('label').text() == grantType) return it }
        browser.waitFor(30) {browser.$(gt)}
        gt.click()

        if(!grantType.matches("Reporter|Project lead|Current assignee")){
            browser.waitFor(10){gtl.$('input')[1].displayed}
            browser.$(gtl).$('input')[1].value(grantValue)
            browser.waitFor(10){browser.$(class:'ajs-layer').$('li').displayed}
            browser.$(class:'ajs-layer').$('li').click()
        }

        browser.$(id:'grant-permission-dialog-grant-button').click()

        return true
    }

    boolean checkWebSudo(){
        def b = getBrowser()
        def response = getSudo(b)
        b.quit()
        return response
    }

    void doLogin(Browser b){
        if (SsoLogin.doSsoLogin(b, jiraServer.getAdmin(), jiraServer.getPassword(), jiraServer.getSsoidl(), jiraServer.getSsoidp())){
            if(b.isAt(WebSudoPage)){
                doWebSudo(b)
                return
            }
            //b.waitFor(10) {LoginPage.form}
            if(b.isAt(LoginPage))
            {
                b.page(LoginPage).username.value(jiraServer.getAdmin())
                b.page(LoginPage).password.value(jiraServer.getPassword())
                b.page(LoginPage).loginButton.click()
            }
        }
    }

    void doWebSudo(b) {
        if (b.isAt(WebSudoPage)){
            b.page(WebSudoPage).password.value(jiraServer.getPassword())
            b.page(WebSudoPage).loginButton.click()
        }
    }

    List<JiraPermission> fillPermissionScheme(Browser b){
        def jps = new ArrayList()
        b.waitFor(10){b.$('tbody tr')}
        b.$('tbody tr').each { row->
            def jp = new JiraPermission(row.$(class:'title').text())
            def grType = ""
            List grVals = null
            row.$('td > dl').children().each {
                if (it.tag() == 'dt'){
                    //println "tag: "+it.text()
                    grType = it.text()
                    grVals = new ArrayList<String>()
                    jp.grants.put(grType.toLowerCase(), grVals)
                }else{
                    if (grType != ""){
                        //ln "put in gr: "+grType+", "+it.text()
                        if (grVals != null){
                            grVals.add(it.text())
                        }
                        //jp.grants.put(grType.toLowerCase(), it.text())
                    }
                }
            }
            jps.add(jp)
        }
        return jps
    }

    JiraServer getJiraServer() {
        return jiraServer
    }

    List<JiraPermissionScheme> getPermissionSchemes(){
        def b = getBrowser()
        if (getSudo(b)){
            b.go PermissionSchemesPage.url
            if ( b.waitFor(30, 1) { b.$('tbody').displayed }){
                if (b.isAt(PermissionSchemesPage)){
                    //println "try get all rows in table"
                    jiraServer.cleanPermissionSchemes()
                    //go all rows
                    b.page(PermissionSchemesPage).table.$('tr').each{ row->
                        def permScheme = new JiraPermissionScheme(
                                row.$('.title > a').attr('href').split('=')[1],
                                row.$('.title > a').text(),
                                row.$('.description > small').text()
                        )
                        //each projects column
                        if (row.$('td')[1].$('a') != null){
                            row.$('td')[1].$('a').each { pr->
                                //def project = new JiraProject(pr.text())
                                def proj = jiraServer.getProjects()?.find { it.name == pr.text() }
                                if(proj == null){
                                    proj = new JiraProject(pr.text())
                                    jiraServer.addProject(proj)
                                }
                                permScheme.addProject(proj)
                            }
                        }
                        jiraServer.addPermissionScheme(permScheme)
                        //listPerm.add(permScheme)
                    }

                    if (JiraPermissionScheme.permissionTypes == null){
                        JiraPermissionScheme.permissionTypes = new ArrayList<>()
                        def firstScheme = jiraServer.getPermissionSchemes().get(0).getId()//listPerm.get(0).getId()
                        //go to permission types
                        b.go PermissionSchemePage.url+firstScheme
                        if (b.isAt(PermissionSchemePage)){

                            b.page(PermissionSchemePage).permissions.each{ permission->
                                JiraPermissionScheme.permissionTypes.add(permission.text())
                            }

                            //go to grant types
                            if (JiraPermission.grantTypes == null){
                                JiraPermission.grantTypes = new ArrayList<>()
                                def edit = b.page(PermissionSchemePage).edits?.find{ it.text() == "Edit" }

                                edit.click()
                                //click expand list
                                b.waitFor(10) { b.$(id:'security-type-list-more-opts-btn') }
                                b.$(id:'security-type-list-more-opts-btn').click()
                                b.$('.radio > label').each { grant->
                                    JiraPermission.grantTypes.add(grant.text())
                                }
                            }
                        }
                    }
                    //b.quit()

                }
            }
        }
        b.quit()
        return jiraServer.getPermissionSchemes()
    }

    boolean getSudo(Browser b){
        b.go GeneralPage.url
        doLogin(b)
        doWebSudo(b)
        if ( b.waitFor(30,1){b.$(id:'options_table').displayed} ){
            return b.isAt(GeneralPage)
        }else{
            return false
        }
    }

    Browser getBrowser(){
        def d = new FirefoxDriver()
        def b = new Browser()
        b.setDriver(d)
        b.setBaseUrl( jiraServer.getAddress() )
        return b
    }

    Set<JiraProject> getProjects(){
        Browser browser = getBrowser()
        browser.go GeneralPage.url
        doLogin(browser)
        browser.go browser.getBaseUrl()+"rest/api/latest/project"

        //
        if (browser.driver instanceof FirefoxDriver){
            browser.waitFor(10) { browser.$("a", id: "rawdata-tab").displayed }
            browser.$("a", id: "rawdata-tab").click()
        }

        browser.waitFor(30) { browser.$("pre", class: "data").displayed }

        def json = new JsonSlurper().parseText(browser.$("pre", class: "data").text())
        //println "json: "+json
        //Set projects = []
        json.each{ jproj->
            def proj = jiraServer.getProjects()?.find{ it.name == jproj.name }
            if (proj == null){
                proj = new JiraProject(jproj.name, jproj.key, jproj.id)
            }
            jiraServer.addProject(proj)
        }

        browser.go RolesPage.url
        doWebSudo(browser)
        browser.waitFor(30) { browser.$( id:'project_roles').displayed }
        browser.page(RolesPage).rows.each {row->
            jiraServer.addRole(row.$('td')[0].text())
        }
        browser.quit()

        return jiraServer.getProjects()
    }

    boolean hasPermission(permSchemeId, permType, grantType, grantValue=null){
        if (jiraServer.permissionSchemes){
            def permSch = jiraServer.permissionSchemes?.find{ it.id == permSchemeId }
            if (permSch){
                def perm = permSch.permissions?.find{ it.name == permType }
                if (perm.grants){

                    def grants = perm.grants.getAt(grantType.toLowerCase())
                    if (grants){
                        //if value not set we apologize for us enough only grant type
                        if ( grantValue == null || grants?.find{ it == grantValue }){
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
}
