package ru.andrewsite.japi.jira;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JiraServer {
    private String address;
    private String admin;
    private String password;
    private String ssoidl;
    private String ssoidp;
    private Set<JiraProject> projects;
    private List<JiraPermissionScheme> permissionSchemes;
    private Set<String> roles;
    //todo type grants per jira

    public JiraServer(String address, String admin, String password, String ssoidl, String ssoidp){
        this.address = address;
        this.admin = admin;
        this.password = password;
        this.ssoidl = ssoidl;
        this.ssoidp = ssoidp;
    }

    public Set<JiraProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<JiraProject> projects) {
        this.projects = projects;
    }

    public void addProject(JiraProject project){
        if(this.projects == null){
            this.projects = new HashSet<>();
        }
        this.projects.add(project);
    }

    public String getSsoidl() {
        return ssoidl;
    }

    public void setSsoidl(String ssoidl) {
        this.ssoidl = ssoidl;
    }

    public String getSsoidp() {
        return ssoidp;
    }

    public void setSsoidp(String ssoidp) {
        this.ssoidp = ssoidp;
    }

    public List<JiraPermissionScheme> getPermissionSchemes() {
        return permissionSchemes;
    }

    public void setPermissionSchemes(List<JiraPermissionScheme> permissionSchemes) {
        this.permissionSchemes = permissionSchemes;
    }

    public void addPermissionScheme(JiraPermissionScheme permissionScheme){
        if(this.permissionSchemes == null){
            this.permissionSchemes = new ArrayList<>();
        }
        this.permissionSchemes.add(permissionScheme);
    }

    public void cleanPermissionSchemes(){
        this.permissionSchemes = null;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdmin() {
        return this.admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public void addRole(String role){
        if (this.roles == null){
            this.roles = new HashSet<>();
        }
        this.roles.add(role);
    }
}
