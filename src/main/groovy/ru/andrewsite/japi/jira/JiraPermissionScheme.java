package ru.andrewsite.japi.jira;

import java.util.ArrayList;
import java.util.List;

public class JiraPermissionScheme {
    private String  id;
    private String name;
    private String description;
    private List<JiraProject> projects;
    private List<JiraPermission> permissions;

    public static List<String> permissionTypes;

    public JiraPermissionScheme(String id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public JiraPermissionScheme(String id){
        this.id = id;
    }

    public void addProject(JiraProject jiraProject){
        if (this.projects == null) { this.projects = new ArrayList<>(); }
        this.projects.add(jiraProject);
    }

    public List<JiraProject> getProjects(){
        return this.projects;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<JiraPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<JiraPermission> permissions) {
        this.permissions = permissions;
    }

}
