package ru.andrewsite.japi.jira;

public class JiraProject {

    private String name;
    private String key;
    private String id;

    public JiraProject(String name){
        this.name = name;
    }

    public JiraProject(String name, String key, String id){
        this.name = name;
        this.key = key;
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
