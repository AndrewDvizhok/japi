package ru.andrewsite.japi.jira;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JiraPermission {
    private String name;
    private Map<String, List<String>> grants;
    public static List<JiraGrant> grantTypes;

    JiraPermission(String name){
        this.name = name;
        this.grants = new HashMap<>();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, List<String>> getGrants() {
        return grants;
    }

    public void setGrants(Map<String, List<String>> grants) {
        this.grants = grants;
    }

    public static List<JiraGrant> getGrantTypes() {
        return grantTypes;
    }

    public static void setGrantTypes(List<JiraGrant> grantTypes) {
        JiraPermission.grantTypes = grantTypes;
    }
}
