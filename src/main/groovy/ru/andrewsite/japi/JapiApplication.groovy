package ru.andrewsite.japi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import ru.andrewsite.japi.jira.JiraServer

@SpringBootApplication
class JapiApplication {

	static void main(String[] args) {
		SpringApplication.run(JapiApplication, args)
	}

}
