package ru.andrewsite.japi;

import com.sun.tools.javac.util.DefinedBy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;
import ru.andrewsite.japi.jira.JiraPermission;
import ru.andrewsite.japi.jira.JiraPermissionScheme;
import ru.andrewsite.japi.jira.JiraProject;
import ru.andrewsite.japi.jira.JiraServer;
import ru.andrewsite.japi.provider.ApiProvider;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class WebController {
    @GetMapping("/set")
    public String set(HttpSession session) {
        //model.addAttribute("set");
        return "set";
    }

    @GetMapping("/permissions")
    public String permissions(
                                @RequestParam(name="c", required = false) String command,
                                HttpSession session,
                                Model model
                                ) {
        //System.out.println("Status of provider: "+session.getAttribute("provider"));
        if (session.getAttribute("provider") == null){
            return "redirect:/set";
        }
        //System.out.println("command: "+command);
        ApiProvider provider = (ApiProvider) session.getAttribute("provider");
        List<JiraPermissionScheme> jps;
        if (command.equals("refresh") || provider.getJiraServer().getPermissionSchemes() == null){
            System.out.println("update schemes: "+command);
            jps = provider.getPermissionSchemes();
        }else{
            jps = provider.getJiraServer().getPermissionSchemes();
        }


        if (jps != null){
            model.addAttribute("jps", jps);
        }else{
           // System.out.println("jps is empty!");
            model.addAttribute("error", "JPS is empty!");
        }
        //if we got list of the types permissions
        if (JiraPermissionScheme.permissionTypes != null){
            model.addAttribute("ptypes", JiraPermissionScheme.permissionTypes);
        }
        //if we got list of grant types of the permission
        if (JiraPermission.grantTypes != null){
            model.addAttribute("gtypes", JiraPermission.grantTypes);
        }
        return "permissions";
    }

    @PostMapping("/permissions")
    public String bulkPerm(
                        @RequestParam Map<String,String> allParams,
                        @RequestParam(name="operation") String operation,
                        @RequestParam(name="grantType", required = false) String grantType,
                        @RequestParam(name="grantValue", required = false) String grantValue,
                        @RequestParam(name="permTypeConds") String[] permTypeConds,
                        @RequestParam(name="grantTypeCond") String grantTypeCond,
                        @RequestParam(name="grantValueCond", required = false) String grantValueCond,
                        HttpSession session
                ){
        ApiProvider provider = (ApiProvider) session.getAttribute("provider");
        List<String> schemeIds = new ArrayList<>();
        allParams.forEach( (k ,v)->{
            if(k.contains("pschem")){
                if ( v.contains("on")) schemeIds.add(k.split("-")[1]);
            }
        } );
        /*if (!operation.contentEquals("delete")){
            if(grantType == null || grantValue == null){
                //send error
            }
        }
        if (schemeIds.size()==0 || permTypeConds.length == 0 ){
            //send error
        }*/
        //System.out.println("Receive schemes for operate: "+schemeIds.size());
        //schemeIds.forEach( (id)->{
            //System.out.println("add perm: "+id);
            //System.out.println("operatopm: "+operation);
            switch (operation){
                case "add":
                    provider.addPermission(schemeIds, grantType, grantValue, permTypeConds, grantTypeCond, grantValueCond);
                    break;
                    //todo update and delete
            }

        //});

        return "permcompare";
    }

    @GetMapping("/projectroles")
    public String projectroles(
                                @RequestParam(name="c", required = false) String command,
                               HttpSession session,
                               Model model
    ) {
        if (session.getAttribute("provider") == null){
            return "redirect:/set";
        }
        ApiProvider provider = (ApiProvider) session.getAttribute("provider");

        model.addAttribute("projects", provider.getProjects());
        model.addAttribute("roles",provider.getJiraServer().getRoles());
        return "projectroles";
    }


}
