package ru.andrewsite.japi;

import groovy.json.JsonBuilder;


import org.json.JSONObject;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.andrewsite.japi.jira.JiraServer;
import ru.andrewsite.japi.provider.ApiProvider;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
public class JapiRestController {
    @PostMapping("/set")
    @ResponseBody
    public Map set(
            @RequestParam(name="address") String address,
            @RequestParam(name="admin") String admin,
            @RequestParam(name="password") String password,
            @RequestParam(name="ssoidl", required = false) String ssoidl,
            @RequestParam(name="ssoidp", required = false) String ssoidp,
            HttpSession session

    ) {
        //create provider if necessary
        if (session.getAttribute("provider") == null){
            session.setAttribute("provider",new ApiProvider(new JiraServer(address, admin, password, ssoidl, ssoidp)));
        }

        //check that server permit access
        ApiProvider provider = (ApiProvider) session.getAttribute("provider");
        //JSONObject jo = new JSONObject();
        Map jo = new HashMap<>();



        if (provider.checkWebSudo()){
            jo.put("status", "OK");
        }else{
            jo.put("status", "Error");
        }
        //System.out.println(jo.toString());
        return jo;
    }

}
