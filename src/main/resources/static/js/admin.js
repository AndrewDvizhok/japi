
function setParameters(){
    let req = new XMLHttpRequest();
    var fd = new FormData();
    req.open("POST", '/set', true);
    //todo JAPI-22
    //req.setRequestHeader('X-CSRF-TOKEN', 'todo xcsrf token');
    //req.send('{ "address": "'+$('#hostJira').val()+'", "password":"'+$('#password').val()+'","user":"'+$('#login').val()+'" }');
    fd.append('address',$('#hostJira').val());
    fd.append('admin', $('#login').val());
    fd.append('password',$('#password').val());
    if ($('#ssoCb').prop('checked')){
        fd.append('ssoid',$('#sso_id_login').val());
        fd.append('ssoid',$('#sso_id_pass').val());
    }

    req.send(fd);

    req.onload = function() {
        if(req.status == 200){
            console.log('ok')
        }else{
            console.log('error')
            console.log(req.response)
        }
    };

    req.onloadend = function(){
        console.log(req.response);
        console.log(req.responseText);
        getResponse((req.status == 200) && (JSON.parse(req.response).status == "OK"));
    }

}

function getResponse(status){
    if(status){
        $('#response').removeClass('d-none');
        $('#response .jumbotron').addClass('bg-success');
        $('#response  h3').text('Connection successfull!');
    }else{
        $('#response').removeClass('d-none');
        $('#response .jumbotron').addClass('bg-danger');
        $('#response  h3').text('Error!');
    }
}

function setSso(){
    if ($('#ssoCb').prop('checked')){
        $('#sso').show();
    }else{
        $('#sso').hide();
    }
}
